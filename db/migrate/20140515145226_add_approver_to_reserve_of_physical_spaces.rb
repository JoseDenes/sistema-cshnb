class AddApproverToReserveOfPhysicalSpaces < ActiveRecord::Migration
  def change
    add_column :reserves_of_physical_space, :approver, :integer
  end
end
