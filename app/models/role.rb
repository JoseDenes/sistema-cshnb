class Role < ActiveRecord::Base
  has_many :users, dependent: :destroy
  attr_accessible :name, :users_attributes
end
