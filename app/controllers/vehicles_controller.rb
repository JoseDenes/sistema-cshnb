class VehiclesController < ApplicationController
    
  include UsersHelper

  #before_filter :autorizacao_chefe_setor_transporte, except: []

  def index
    @vehicles = Vehicle.where(deleted: false)    
  end

  def show
    @vehicle = Vehicle.find(params[:id])    
  end

  def new
    @vehicle = Vehicle.new   
  end

  def edit
    @vehicle = Vehicle.find(params[:id])
  end

  def create
    @vehicle = Vehicle.new(params[:vehicle])
    @vehicle.deleted = false
  
    if @vehicle.save
      redirect_to vehicles_path
    
    else
      render :new
    end 
  end

  def update
    @vehicle = Vehicle.find(params[:id])

    if @vehicle.update_attributes(params[:vehicle])
      redirect_to vehicles_path
    else
      render :edit
    end
  end

  def destroy
    @vehicle = Vehicle.find(params[:id])
    @vehicle.update_attributes(deleted: true)
    redirect_to vehicles_path
  end
end
