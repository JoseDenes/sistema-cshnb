class ReservationStatusesController < ApplicationController

  def index
    @reservation_statuses = ReservationStatus.where(deleted: false)
  end

  def show
    @reservation_status = ReservationStatus.find(params[:id])
  end

  def new
    @reservation_status = ReservationStatus.new
  end

  def edit
    @reservation_status = ReservationStatus.find(params[:id])
  end

  def create
    @reservation_status = ReservationStatus.new(params[:reservation_status])
    @reservation_status.deleted = false
    
    if @reservation_status.save
      redirect_to reservation_statuses_path  
    else
      render :new
    end
  end

  def update
    @reservation_status = ReservationStatus.find(params[:id])

    if @reservation_status.update_attributes(params[:reservation_status])
      redirect_to reservation_statuses_path  
    else
      render :edit  
    end
  end

  def destroy
    @reservation_status = ReservationStatus.find(params[:id])
    @reservation_status.update_attribute(:deleted, true)
    redirect_to reservation_statuses_path
  end
end
